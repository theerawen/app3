package com.theeramed.app3

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.theeramed.app3.databinding.ActivityMainBinding
import com.theeramed.app3.model.Oil

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        //data
        val data = listOf<Oil>(
            Oil("เซลล์ ฟิวเซฟ แก๊สโซฮอล์ E20", 36.84),
            Oil("เซลล์ ฟิวเซฟ เเก๊สโซฮอล91", 37.68),
            Oil("เซลล์ ฟิวเซฟ เเก๊สโซฮอล95", 37.95),
            Oil("เซลล์ วี-เพาเวอร์ เเก๊สโซฮอล91", 45.44),
            Oil("เซลล์ ดีเซล B20", 36.34),
            Oil("เซลล์ ฟิวเซฟ ดีเซล", 36.34),
            Oil("เซลล์ ฟิวเซฟ ดีเซล B7", 36.34),
            Oil("เซลล์ วี-เพาเวอร์ ดีเซล", 36.34),
            Oil("เซลล์ วี-เพาเวอร์ ดีเซล B7", 47.06)

        )

//       recyclerView
        val recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ItemAdapter(data, applicationContext)
    }

    class ItemAdapter(val data: List<Oil>, val context: Context) :
        RecyclerView.Adapter<ItemAdapter.ViewHolder>() {


        inner class ViewHolder(private val itemView: View) : RecyclerView.ViewHolder(itemView) {
            val title = itemView.findViewById<TextView>(R.id.item_title)
            val price = itemView.findViewById<TextView>(R.id.item_price)
            val item = itemView.findViewById<LinearLayout>(R.id.item)
        }

        //จังหวะ recycler มัน bind กับ item_list
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val adapterLayout =
                LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
            return ViewHolder(adapterLayout)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            var item = data[position]
            holder.title.text = item.title
            holder.price.text = item.price.toString()
            holder.item.setOnClickListener {
                Toast.makeText(
                    context,
                    "${item.price.toString()} ${item.title}", Toast.LENGTH_LONG
                )
                    .show()
            }
        }

        override fun getItemCount(): Int {
            return data.size
        }
    }
}

